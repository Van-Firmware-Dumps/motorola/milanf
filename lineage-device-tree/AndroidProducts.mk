#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_milanf.mk

COMMON_LUNCH_CHOICES := \
    lineage_milanf-user \
    lineage_milanf-userdebug \
    lineage_milanf-eng
